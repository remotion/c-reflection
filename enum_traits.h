//===----------------------------------------------------------------------===//
//  (C) Copyright:  Andrew Tomazos, Christian Kaeser, Remotion (I. Schulz) 2013-2014
//  http://www.remotion4d.net
//
//	Compile-time Reflection for C++
//
//  Use, modification and distribution are subject to the Boost Software License,
//  Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt).
//===----------------------------------------------------------------------===//
// 
// http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3815.html
// 
//===----------------------------------------------------------------------===//

#pragma once

//#include <type_traits>
//#include <integer_sequence>

/*
  Used compiler intrinsics
__enumerator_list_size(Enum) -> size_t              //number of enumerators in Enum
__enumerator_value(Enum,I) -> size_t                //value of I'th enumerator in Enum
__enumerator_identifier(Enum,I) -> string literal   //identifier of I'th enumerator in Enum
*/

namespace std
{

template<typename E>
struct enumerator_list_size
{
	// static_assert(std::is_enum<E>::value, "E not enum type");
	enum { 	value = __enumerator_list_size(E)	}; //int
};

template<typename E, unsigned int I>
struct enumerator_identifier
{
	// static_assert(std::is_enum<E>::value, "E not enum type");
	// static_assert(0 <= I && I < std::enumerator_list_size<E>::value, "I out-of-bounds");
	static constexpr decltype(auto) value = __enumerator_identifier(E, I); //string literal
};

template<typename E, unsigned int I>
struct enumerator_value //: std::integral_constant<E, __enumerator_value(E, I)>
{
	// static_assert(std::is_enum<E>::value, "E not enum type");
	// static_assert(0 <= I && I < std::enumerator_list_size<E>::value, "I out-of-bounds");
	enum { 	value = __enumerator_value(E, I)	}; //int
};


} // namespace std


namespace meta {

template<typename E, class IdxSeq>
struct enumerator_data_impl;

template<typename E, size_t... Idx>
struct enumerator_data_impl<E, std::index_sequence<Idx...>> {
	static constexpr size_t count = std::enumerator_list_size<E>::value;
	static constexpr int values[] = { std::enumerator_value<E, Idx>::value... };
	static constexpr char const *names[] = { std::enumerator_identifier<E, Idx>::value... };
};

template<typename E, size_t... Idx> 
constexpr int enumerator_data_impl<E, std::index_sequence<Idx...>>::values[];

template<typename E, size_t... Idx> 
constexpr char const * enumerator_data_impl<E, std::index_sequence<Idx...>>::names[];

template<typename E>
using enumerator_data = enumerator_data_impl<E, std::make_index_sequence< std::enumerator_list_size<E>::value >>;

}; //namespace meta

