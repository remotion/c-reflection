//===----------------------------------------------------------------------===//
//  (C) Copyright:  Remotion (I. Schulz) 2013-2014
//  http://www.remotion4d.net
//
//	Compile-time Reflection for C++
//
//  Use, modification and distribution are subject to the Boost Software License,
//  Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt).
//===----------------------------------------------------------------------===//

#pragma once


/*
Used compiler intrinsics
__record_friend_count(T) -> size_t
__record_friend_type(T,I) -> Type
__record_friend_identifier(T,I) -> string literal
*/

namespace meta {

	template<class R>
	struct friend_list_size {
		//static_assert(is_class_or_union<R>::value, "R not class type");

		enum { value = __record_friend_count(R) }; //int
	};

	template<class R, unsigned int I>
	struct friend_identifier {
		//static_assert(std::is_class<R>::value || std::is_union<R>::value, "R not class type");
		static_assert(0 <= I && I < friend_list_size<R>::value, "I out-of-bounds");

		static constexpr decltype(auto) value = __record_friend_identifier(R, I); //string literal
	};

	template<class R, unsigned int I>
	struct friend_type {
		//static_assert(std::is_class<R>::value || std::is_union<R>::value, "R not class type");
		static_assert(0 <= I && I < friend_list_size<R>::value, "I out-of-bounds");

		typedef __record_friend_type(R, I) type; //Type
	};

	/// friend_data
	template<class R, class IdxSeq>
	struct friend_data_impl;

	template<class R, size_t... Idx>
	struct friend_data_impl<R, std::index_sequence<Idx...>>
	{
		//type of this record (class/struct/union)
		typedef		R		type;

		//how many member functions are int this record
		static constexpr size_t count = friend_list_size<R>::value;

		//names of all non static member functions.
		static constexpr char const *names[] = { friend_identifier<R, Idx>::value... };

		//all types 
		using types = std::tuple< __record_friend_type(R, Idx)... >;  //abuse tuple as type list ???
	};

	template<class R, size_t... Idx>
	constexpr char const* friend_data_impl<R, std::index_sequence<Idx...>>::names[];

	template<class R>
	struct friend_data : public friend_data_impl<R, std::make_index_sequence< friend_list_size<R>::value >>
	{};

}; //namespace meta