//===----------------------------------------------------------------------===//
//  (C) Copyright:  Remotion (I. Schulz) 2013-2014
//  http://www.remotion4d.net
//
//	Compile-time Reflection for C++
//
//  Use, modification and distribution are subject to the Boost Software License,
//  Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
//  http://www.boost.org/LICENSE_1_0.txt).
//===----------------------------------------------------------------------===//

#pragma once

#include <type_traits>
#include <tuple>  // abusing tuple as type list.

#include "integer_sequence.h" //for c++14 this is in <utility> header
#include "meta_info_enum.h"
#include "enum_traits.h"
#include "base_traits.h"
#include "method_traits.h"
#include "field_traits.h"
#include "friend_traits.h"

namespace meta {

/*
 Used compiler intrinsics
__type_canonical_name(Type) -> string literal       //canonical name of the Type
__type_sugared_name(Type) -> string literal         //sugared name of the Type
__type_is_unnamed(Type) -> bool                     //checks if a Type is unnamed
*/

template<typename T>
struct type_identifier {
	static constexpr decltype(auto) value = __type_canonical_name(T); 
};

}; //namespace meta



namespace meta {

	//===----------------------------------------------------------------------===//
/// Convert class|struct into to tuple
//  struct { int a, float b; }  will be converted to tuple<int,float> 
template<typename R, std::size_t... I>
constexpr auto any2tuple_impl(const R& r, std::index_sequence<I...>) -> decltype(auto)
{
	return std::tuple< meta::field_type_t<R, I>... >(
		meta::field_ref<R, I>(r)...
		);
}

template<typename R, typename Indices = std::make_index_sequence< meta::field_list_size<R>::value > >
constexpr auto any2tuple(const R& r) -> decltype(auto)
{
	return any2tuple_impl(r, Indices());
}


//===----------------------------------------------------------------------===//
/// Convert class|struct into to reference tuple
//  struct { int a, float b; }  will be converted to tuple<int &,float &> 
template<typename R, std::size_t... I>
constexpr auto as_ref_tuple_impl(R&& r, std::index_sequence<I...>) -> decltype(auto)
{
	return std::tie(meta::field_ref<R, I>(r)...);
}

template<typename R, typename Indices = std::make_index_sequence< meta::field_list_size<R>::value > >
constexpr auto as_ref_tuple(R&& r) -> decltype(auto)
{
	return as_ref_tuple_impl(r, Indices());
}


//c++14 only constexpr strcmp
inline constexpr int
strcmp_ce(const char s1[], const char s2[])
{
	for (; *s1 == *s2; s1++, s2++)
	if (*s1 == '\0')
		return 0;
	return ((*s1 < *s2) ? -1 : +1);
}

//===----------------------------------------------------------------------===//
//Check if a class has member function or field by name.
template<class R> inline constexpr bool
has_field(const char ident[]) {
	typedef meta::field_data< R > FDR;
	for (int i = 0; i < FDR::count; ++i) {
		if (strcmp_ce(FDR::names[i], ident) == 0)
			return true;
	}
	return false;
}

template<class R> inline constexpr bool
has_method(const char ident[]) {
	typedef meta::method_data< R > MDR;
	for (int i = 0; i < MDR::count; ++i) {
		if (strcmp_ce(MDR::names[i], ident) == 0)
			return true;
	}
	return false;
}

template<class R>
inline constexpr bool
has_member(const char ident[]){
	return has_field<R>(ident) || has_method<R>(ident);
}


template<class R>
inline constexpr int
field_index(const char ident[])
{
	typedef meta::field_data< R > fdR;
	for (int i = 0; i < fdR::count; ++i) {
		if (strcmp_ce(fdR::names[i], ident) == 0)
			return i;
	}
	return -1;
}

template<class R>
inline constexpr int
method_index(const char ident[])
{
	typedef meta::method_data< R > mdR;
	for (int i = 0; i < mdR::count; ++i) {
		if (strcmp_ce(mdR::names[i], ident) == 0)
			return i;
	}
	return -1;
}

//===----------------------------------------------------------------------===//
//Struct-of-Arrays vector.
template<class R, template<typename T> class ContainerType, class IdxSeq>
struct as_soa_impl;

template<class R, template<typename T> class ContainerType, size_t... Idx>
struct as_soa_impl<R, ContainerType, std::index_sequence<Idx...>> {
	typedef std::tuple< ContainerType< meta::field_type_t<R, Idx> >... >   type;
};

template<typename T>
using vector_def_alloc = std::vector<T, std::allocator<T>>;

template<typename R, template<typename T> class ContainerType = vector_def_alloc >
struct as_soa : public as_soa_impl<R, ContainerType, std::make_index_sequence< meta::field_list_size<R>::value >>
{};

}; //namespace meta


