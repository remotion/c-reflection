Compile-time Reflection for C++
===============================

Please note that this is work in progress.
Let me know if something can be improved or done in a better way.

Requrements
-----------

No RTTI, assume that the code is compiling  using ( -no-rtti, /GR- ) flag.

Exception handling is not required, assume that the code is compiling with (-fno-exceptions) flag.

No changes to the language it self, it is still C++14.

Only some compiler magic, just like type_traits already using but much more powerfull.

What can be done already?
-------------------------
Asses information about Enumerations.
Asses information about Classes, Structs and Unions.
Including class bases, field, methods and even parameter identifier.

Next step.
----------
Namespace introspection...

How does it work?
-----------------
It works by adding some compiler intrinsics to the compiler (Clang). 


LLVM toolchain for Visual Studio
--------------------------------
[Alpha-build website](http://llvm.org/builds/)

[Download: Clang Reflect for MSVC 2013](https://bitbucket.org/remotion/c-reflection/downloads/Clang_MSVC.7z)

You may need to use this flags (/GR- /D_HAS_EXCEPTIONS=0 /MTd) in Visual Studio 2013.

[Clang-reflect compiler source code](https://bitbucket.org/remotion/clang-reflect)

Examples 
-----------



### Print names of all (non static) methods
```
#!cpp
class test_class
{
public:
	int f1() { return 0; };
	int f2() const { return 0; };
	int f3() volatile { return 0; };
	int f4() & { return 0; };
	int f5() && { return 0; };
	virtual int f6() = 0;
	virtual int f7() { return 0; };
};

/// print names of all (non static) methods in test_class
typedef meta::method_data< test_class >   tc_data;
for (const auto& name : tc_data::names){
	printf(" method %s \n", name);
}
```


### Universal compare operator
```
#!cpp
struct S {
	int i;
	double d;
	char c;
	std::array<char,8> s;
};

constexpr bool operator < (const S& l, const S& r) {
	return meta::as_ref_tuple(l) < meta::as_ref_tuple(r);
}

```

### Check if a class has member function or field by name.
```
#!cpp
template<class R> inline constexpr bool 
has_field(const char ident[]) {
	typedef meta::field_data< R > FDR;
	for (int i = 0; i < FDR::count; ++i) {
		if (strcmp_ce(FDR::names[i], ident) == 0)
			return true;
	}
	return false;
}

template<class R> inline constexpr bool 
has_method(const char ident[]) {
	typedef meta::method_data< R > MDR;
	for (int i = 0; i < MDR::count; ++i) {
		if (strcmp_ce(MDR::names[i], ident) == 0)
			return true;
	}
	return false;
}

template<class R>
inline constexpr bool
has_member(const char ident[]){
	return has_field<R>(ident) || has_method<R>(ident);
}
```

```
#!cpp
class ClassA {
	int   m_i;
	float m_f;

	int get_i() { return m_i; }
	int get_f() { return m_f; }
};
static_assert_exp(has_field<ClassA>("m_i"));
static_assert_exp(has_field<ClassA>("m_f"));
static_assert_exp(!has_field<ClassA>("m_no"));

static_assert_exp(has_method<ClassA>("get_i"));
static_assert_exp(has_method<ClassA>("get_f"));
static_assert_exp(!has_method<ClassA>("get_no"));

```


### The Struct-of-Arrays vector.
```
#!cpp
template<class R, template<typename T> class ContainerType, class IdxSeq>
struct as_soa_impl;

template<class R, template<typename T> class ContainerType, size_t... Idx>
struct as_soa_impl<R, ContainerType, std::index_sequence<Idx...>> {
	typedef std::tuple< ContainerType< meta::field_type_t<R, Idx> >... >   type;
};

template<typename T>
using vector_def_alloc = std::vector<T, std::allocator<T>>;

template<typename R, template<typename T> class ContainerType = vector_def_alloc >
struct as_soa : public as_soa_impl<R, ContainerType, std::make_index_sequence< meta::field_list_size<R>::value >>
{};

```