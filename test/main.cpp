///Some workarounds for clang-cl.exe
#if  __clang__

#define _ISTREAM_  //disable #include <istream> 
#define _TYPEINFO_ //disable #include <typeinfo> 

//disable STL exceptions >>>
#define _HAS_EXCEPTIONS 0

//disable iterator debug
#define _ITERATOR_DEBUG_LEVEL 0

#include <yvals.h>

//we have rvalue reference for *this support
#undef  _HAS_REF_QUALIFIER
#define _HAS_REF_QUALIFIER 1

//we have noexcept support 
#undef  _NOEXCEPT
#define _NOEXCEPT noexcept 

#endif //__clang__ 

#include <utility>
#include <type_traits>
#include <initializer_list>
#include <tuple>
#include <vector>
#include <array> 

#include "../meta.h"

#define static_assert_exp(exp) static_assert(exp,#exp)


///Some basic test cases for c++ reflection...

//===----------------------------------------------------------------------===//
class test_class
{
public:
	int f1() { return 0; };
	int f2() const { return 0; };
	int f3() volatile { return 0; };
	int f4() & { return 0; };
	int f5() && { return 0; };
	virtual int f6() = 0;
	virtual int f7() { return 0; };
};
static_assert_exp((meta::method_info<test_class, 0>::is_public()));

/// Universal compare operator
struct S {
	int i;
	double d;
	char c;
	std::array<char, 8> s;
};

bool operator < (const S& l, const S& r) {
	return meta::as_ref_tuple(l) < meta::as_ref_tuple(r);
}


//===----------------------------------------------------------------------===//
/// Check if a class has member function or field by name.
class ClassA {
	int   m_i;
	float m_f;

	int get_i() { return m_i; }
	int get_f() { return m_f; }
};
static_assert_exp( meta::has_field<ClassA>("m_i"));
static_assert_exp( meta::has_field<ClassA>("m_f"));
static_assert_exp(!meta::has_field<ClassA>("m_no"));

static_assert_exp( meta::has_method<ClassA>("get_i"));
static_assert_exp( meta::has_method<ClassA>("get_f"));
static_assert_exp(!meta::has_method<ClassA>("get_no"));



//===----------------------------------------------------------------------===//
int main(int argc, char* argv[])
{
	/// print names of all (non static) methods in test_class
	typedef meta::method_data< test_class >   test_class_meta_data;
	for (const auto& name : test_class_meta_data::names){
		printf(" method %s \n", name);
	}
	static_assert_exp(test_class_meta_data::is_public(0));


	printf("\n Struct-of-Arrays vector. \n");
	printf(" as_soa %s \n", meta::type_identifier< meta::as_soa<S>::type >::value);
	printf(" as_soa %i \n", meta::field_list_size< meta::as_soa<S>::type >::value);
	printf(" as_soa %i \n", meta::bases< meta::as_soa<S>::type >::count);
	printf(" as_soa %i \n", sizeof(meta::as_soa<S>::type));

	///Enumerations 
	printf("\n Enumerations. \n");
	typedef meta::enumerator_data<meta::meta_info_enum>  meta_info_data;
	for (int i = 0; i<meta_info_data::count; ++i) {
		printf(" enum %s = %x \n", meta_info_data::names[i], meta_info_data::values[i]);
	}


	getchar();
	return 0;
}